package com.rybak.task2.bean.otherpackage;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class OtherBeanB implements BaseBean {
}
