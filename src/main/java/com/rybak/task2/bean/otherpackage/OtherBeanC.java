package com.rybak.task2.bean.otherpackage;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("myBeanC")
@Order(1)
public class OtherBeanC implements BaseBean {
}
