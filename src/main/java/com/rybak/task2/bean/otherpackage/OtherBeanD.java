package com.rybak.task2.bean.otherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanD {

    @Autowired
    OtherBeanA otherBeanA;
    @Autowired
    OtherBeanB otherBeanB;

    @Override
    public String toString() {
        return "OtherBeanD{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                '}';
    }
}
