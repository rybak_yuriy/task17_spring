package com.rybak.task2.bean.otherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class OtherBeanF implements BaseBean {

    private BaseBean otherBeanA;

    private BaseBean otherBeanB;

    private BaseBean bean;

    @Autowired
    public void setOtherBeanA(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }

    @Autowired
    public void setOtherBeanB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    @Autowired
    @Qualifier("myBeanC")
    public void setBean(BaseBean bean) {
        this.bean = bean;
    }

    @Override
    public String toString() {
        return "OtherBeanF{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                ", bean=" + bean +
                '}';
    }
}
