package com.rybak.task2.bean.otherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class OtherBeanE implements BaseBean {

    private OtherBeanA otherBeanA;
    private OtherBeanB otherBeanB;

    @Autowired
    public OtherBeanE(OtherBeanA otherBeanA, OtherBeanB otherBeanB) {
        this.otherBeanA = otherBeanA;
        this.otherBeanB = otherBeanB;
    }

    @Override
    public String toString() {
        return "OtherBeanE{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                '}';
    }
}

