package com.rybak.task2.bean.otherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BeansCollection {

    @Autowired
    private List<BaseBean> beanList;

    public void showBeans() {
        System.out.println("\t<---Start Bean List from BeansCollection---> ");
        for (BaseBean bean : beanList) {
            System.out.println(bean.toString());
        }
        System.out.println("\t<---End Bean List from BeansCollection---> ");
    }

}
