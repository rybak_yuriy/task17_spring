package com.rybak.task2.bean.otherpackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanBoss {

    @Autowired
    private OtherBeanA otherBeanA;

    @Autowired
    private BaseBean baseBean;

    @Autowired
    @Qualifier("otherBeanC")
    private BaseBean baseBeanC;

    @Autowired
    @Qualifier("otherBeanE")
    private BaseBean baseBeanE;

    public void showBeans() {
        System.out.println("\t<---Start Bean List from BeanBoss---> ");
        System.out.println(otherBeanA);
        System.out.println(baseBean);
        System.out.println(baseBeanC);
        System.out.println(baseBeanE);
        System.out.println("\t<---End Bean List from BeanBoss---> ");
    }
}
