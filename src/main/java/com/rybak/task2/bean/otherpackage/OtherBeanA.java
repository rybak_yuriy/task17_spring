package com.rybak.task2.bean.otherpackage;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Scope("prototype")
public class OtherBeanA implements BaseBean {
}
