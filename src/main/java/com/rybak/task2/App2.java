package com.rybak.task2;

import com.rybak.task2.bean.otherpackage.*;
import com.rybak.task2.config.ConfigOne;
import com.rybak.task2.config.ConfigThree;
import com.rybak.task2.config.ConfigTwo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App2 {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("two");
        context.register(ConfigOne.class);
        context.register(ConfigTwo.class);
        context.register(ConfigThree.class);
        context.refresh();
        for (String beanName : context.getBeanDefinitionNames()) {
            System.out.println(context.getBean(beanName).getClass().toString());
        }
        System.out.println(context.getBean(OtherBeanD.class).toString());
        System.out.println(context.getBean(OtherBeanE.class).toString());
        System.out.println(context.getBean(OtherBeanF.class).toString());

        context.getBean(BeansCollection.class).showBeans();
        context.getBean(BeanBoss.class).showBeans();

    }


}
