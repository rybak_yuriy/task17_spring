package com.rybak.task2.config;

import com.rybak.task2.bean.otherpackage.OtherBeanA;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.rybak.task2.bean.otherpackage")
public class ConfigThree {

    @Profile("bean")
    @Bean
    public OtherBeanA getBeanA() {
        return new OtherBeanA();
    }
}
