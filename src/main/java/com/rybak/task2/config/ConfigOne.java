package com.rybak.task2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.rybak.task2.bean.beans1")
@Profile("one")
public class ConfigOne {
}
