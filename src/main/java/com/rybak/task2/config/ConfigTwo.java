package com.rybak.task2.config;

import com.rybak.task2.bean.beans2.CatAnimal;
import com.rybak.task2.bean.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan(basePackages = "com.rybak.task2.bean.beans3",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class))
@ComponentScan(basePackages = "com.rybak.task2.bean.beans2",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CatAnimal.class))
@Profile("two")
public class ConfigTwo {
}
