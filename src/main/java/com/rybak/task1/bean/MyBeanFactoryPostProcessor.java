package com.rybak.task1.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
            throws BeansException {
        System.out.println("\t<---Start of BeanDefinitions--->");
        for (String name : factory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = factory.getBeanDefinition(name);
            System.out.println(beanDefinition);
        }
        System.out.println("\t<---End of BeanDefinitions--->");

        BeanDefinition beanBDefinition = factory.getBeanDefinition("BeanB");
        if (beanBDefinition != null) {
            beanBDefinition.setInitMethodName("secondInit");
        }
    }

    private void init() {
        System.out.println(this.getClass().getName() + " init from BeanFactoryPostProcessor  ");
    }
}
