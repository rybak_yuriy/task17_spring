package com.rybak.task1.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanB implements BeanValidator {
    private static final Logger log = LogManager.getLogger(BeanB.class);

    private String name;
    private int value;

    public BeanB() {
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if (name == null) {
            log.log(Level.ERROR, "Name can't be null " + this.getClass().getName());
        }
        if (value < 0) {
            log.log(Level.ERROR, "Value must be greater then zero " + this.getClass().getName());
        }
        System.out.println(this.getClass().getName() + " validate method (BeanValidator)");
    }

    private void init() {
        System.out.println(this.getClass().getName() + " init method");
    }

    private void secondInit() {
        System.out.println(this.getClass().getName() + " secondInit method");
    }

    private void destroy() {
        System.out.println(this.getClass().getName() + " destroy method");
    }
}
