package com.rybak.task1.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private static final Logger log = LogManager.getLogger(BeanA.class);

    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if (name == null) {
            log.log(Level.ERROR, "Name can't be null " + this.getClass().getName());
        }
        if (value < 0) {
            log.log(Level.ERROR, "Value must be greater then zero " + this.getClass().getName());
        }
        System.out.println(this.getClass().getName() + " validate method (BeanValidator) " + this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println(this.getClass().getName() + " afterPropertiesSet method (InitializingBean) ");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println(this.getClass().getName() + " destroy method (DisposableBean) ");
    }
}
