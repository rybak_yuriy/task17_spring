package com.rybak.task1.bean;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private static final Logger log = LogManager.getLogger(BeanE.class);

    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {
        if (name == null) {
            log.log(Level.ERROR, "Name can't be null " + this.getClass().getName());
        }
        if (value < 0) {
            log.log(Level.ERROR, "Value must be greater then zero " + this.getClass().getName());
        }
        System.out.println(this.getClass().getName() + " validate method (BeanValidator)");
    }

    @PostConstruct
    public void annoInit() {
        System.out.println("inside @PostConstruct-method");
    }

    @PreDestroy
    public void annoDestroy() {
        System.out.println("inside @PreDestroy-method");
    }
}
