package com.rybak.task1.bean;

@FunctionalInterface
public interface BeanValidator {

    void validate();
}
