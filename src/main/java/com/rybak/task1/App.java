package com.rybak.task1;

import com.rybak.task1.config.ConfigTwo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class App {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ConfigTwo.class);

        System.out.println("\t<---Start beans list--->");
        Arrays.stream(context.getBeanDefinitionNames())
                .forEach(System.out::println);
        System.out.println("\t<---End beans list--->");

        context.close();
    }
}
