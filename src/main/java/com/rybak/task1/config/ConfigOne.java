package com.rybak.task1.config;

import com.rybak.task1.bean.BeanB;
import com.rybak.task1.bean.BeanC;
import com.rybak.task1.bean.BeanD;
import com.rybak.task1.bean.BeanF;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("my.properties")
public class ConfigOne {

    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;
    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;
    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;

    @Bean(value = "BeanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "BeanD")
    public BeanB getBeanB() {
        return new BeanB(nameB, valueB);
    }

    @Bean(value = "BeanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = {"BeanD", "BeanB"})
    public BeanC getBeanC() {
        return new BeanC(nameC, valueC);
    }

    @Bean(value = "BeanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD(nameD, valueD);
    }

    @Bean("BeanF")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }


}
