package com.rybak.task1.config;

import com.rybak.task1.bean.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ConfigOne.class)
public class ConfigTwo {

    @Bean("BeanAOne")
    public BeanA getBeanAOne(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB.getName(), beanC.getValue());
    }

    @Bean("BeanATwo")
    public BeanA getBeanATwo(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB.getName(), beanD.getValue());
    }

    @Qualifier("BeanA3")
    @Bean("BeanAThree")
    public BeanA getBeanAThree(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC.getName(), beanD.getValue());
    }

    @Bean("BeanEOne")
    public BeanE getBeanEOne(BeanA BeanAOne) {
        return new BeanE(BeanAOne.getName(), BeanAOne.getValue());
    }

    @Bean("BeanETwo")
    public BeanE getBeanETwo(BeanA BeanATwo) {
        return new BeanE(BeanATwo.getName(), BeanATwo.getValue());
    }

    @Bean("BeanEThree")
    public BeanE getBeanEThree(@Qualifier("BeanA3") BeanA getBeanAThee) {
        return new BeanE(getBeanAThee.getName(), getBeanAThee.getValue());
    }

    @Bean(value = "postProcessor", initMethod = "init")
    public MyBeanFactoryPostProcessor getMyBeanFactoryPostProcessor() {
        return new MyBeanFactoryPostProcessor();
    }

    @Bean
    public MyBeanPostProcessor getMyBeanPostProcessor() {
        return new MyBeanPostProcessor();
    }
}
